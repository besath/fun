#include <sourcemod>
#include <sdktools_sound>

public Plugin myinfo =
{
	name = "Fun with sounds",
	author = "Besath",
	description = "Plays the 'Are we having fun' sound",
	version = SOURCEMOD_VERSION,
	url = "https://bitbucket.org/besath/fun/"
};

public void OnPluginStart() 
{
    LoadTranslations("common.phrases");
    RegConsoleCmd("sm_fun", Command_Fun, "Plays a fun sound to all players or a specific player");
}

public OnConfigsExecuted()
{
    PrecacheSound("soul/sf_fun.wav");
}

// Play the sound even if uppercase chat trigger is used by using sm_fun console command instead (accepts targets)
public Action:OnClientSayCommand(int client, const char[] command, const char[] sArgs)
{
	decl String:chatCommand[128];
	strcopy(chatCommand, sizeof(chatCommand), sArgs);
	StripQuotes(chatCommand);

	if(StrContains(chatCommand, "!fun", true) && IsChatTrigger())
	{
		ReplaceStringEx(chatCommand, sizeof(chatCommand), "!fun", "sm_fun", -1, -1, false);
		FakeClientCommand(client, chatCommand);
	}
	return Plugin_Continue;
}

public Action Command_Fun(int client, int args) 
{
	// Play sound to all players in the server if no name is specified after the command
    if (args < 1)
    {
        EmitSoundToAll("soul/sf_fun.wav", SNDCHAN_AUTO,SNDLEVEL_NORMAL,SND_NOFLAGS,1.0);
        return Plugin_Handled;
    }

    char arg1[64];
    GetCmdArg(1, arg1, sizeof(arg1));

    char target_name[MAX_TARGET_LENGTH];
	int target_list[MAXPLAYERS], target_count;
	bool tn_is_ml;
	
	// Throw an error if target player not found
	if ((target_count = ProcessTargetString(
		arg1,
		client, 
		target_list, 
		MAXPLAYERS, 
		0,
		target_name,
		sizeof(target_name),
		tn_is_ml)) <= 0)
	{
		ReplyToTargetError(client, target_count);
		return Plugin_Handled;
	}

	// Iterate through the players list and play the sound to the specified player if found
	for (int i = 0; i < target_count; i++)
	{
		int target = target_list[i];
		
		EmitSoundToClient(target, "soul/sf_fun.wav", _, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, 1.0);
	}
    return Plugin_Handled;
}